import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header.component';
import {AppRoutingModule} from '../../app-routing.module';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [HeaderComponent],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MatButtonModule
  ]
})
export class HeaderModule {
}
