import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatTooltipModule} from '@angular/material';
import {DialogModule} from '../../components/dialog/dialog.module';

@NgModule({
    declarations: [HomeComponent],
    imports: [
        CommonModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatTooltipModule,
        DialogModule
    ]
})
export class HomeModule {
}
