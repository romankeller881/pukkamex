import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogComponent} from '../../components/dialog/dialog.component';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(public dialog: MatDialog) {
    }

    ngOnInit() {
    }

    openDialog1() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то текст'}
        });
    }

    openDialog2() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog3() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog4() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog5() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то текст'}
        });
    }

    openDialog6() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog7() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog8() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog9() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog10() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog11() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog12() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog13() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }

    openDialog14() {
        this.dialog.open(DialogComponent, {
            width: '600px',
            data: {text: 'какой то другой текст'}
        });
    }
}
