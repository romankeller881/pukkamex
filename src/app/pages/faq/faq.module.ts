import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import {MatExpansionModule} from '@angular/material';



@NgModule({
  declarations: [FaqComponent],
    imports: [
        CommonModule,
        MatExpansionModule
    ]
})
export class FaqModule { }
